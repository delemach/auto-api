'use strict';
//using 'obligate' instead of 'expect' for clarity - supertest uses the word 'expect' to test the api responses.
var obligate = require('chai').expect; 
var apitester = require('supertest');
var config = require('../config').config;
var app = require('../server').app;


describe('GET Entity', function () {
	//TODO: these timeouts are pretty much a hack to give the application time to setup config values and connect to the db.
	this.timeout(4000);
	before(function (done) {
		setTimeout(done, 3000);
	});

	it('errors if it fails to get an entity', function (done) {
		apitester(app)
			.get('/' + config.apiSubpath + '/' + Object.keys(config.models)[0] + 's')
			.expect(200)
			.expect('Content-Type', /json/)
			.end(function (err, res) {
				if (err) { return done(err); }
				obligate(res).to.have.property('body').and.be.instanceof(Array);
				done();
			});
	});

});
