'use strict';
var mongoose = require('mongoose');
var async = require('async');

var validateEmail = function (email) {
    return (/^[a-zA-Z0-9._\-]+@[a-zA-Z0-9.\-]+\.[a-zA-Z]{2,4}$/).test(email);
};

//NOTE: mongoose schema names and swagger model names must match exactly!
//Define Mongoose Schemas. See http://mongoosejs.com/docs/guide.html
var user = new mongoose.Schema({
	firstName: String,
	lastName: String,
    email: {type : String, validate: [validateEmail, 'Email address is not valid.'] }
});

var asset = new mongoose.Schema({
	name: String,
	typeCode: String,
	typeDescription: String,
	purchaseDate: { type: Date, default: Date.now },
	units: Number,
	rating: Number,
	active: Boolean


});


//Define swagger models. See swagger model spec here: https://github.com/wordnik/swagger-core/wiki/Datatypes
//Maybe a mongoose plugin could auto-generate this off the schema or model? That would be much cleaner then having to define everything twice.
var swaggerModels = {
	models: {
		user: {
			id: 'user',
			properties: {
				_id: {
					type: 'string',
					description: 'MongoDB unique ObjectID.'
				},
				firstName: { type: 'string' },
				lastName: { type: 'string' },
				email: { type: 'string' }
			}
		},

		asset: {
			id: 'asset',
			properties: {
				_id: {
					type: 'string',
					description: 'MongoDB unique ObjectID.'
				},
				name: { type: 'string' },
				typeCode: {
					type: 'string',
					description: 'Code for this asset determined by the business.'
				},
				typeDescription: { type: 'string' },
				purchaseDate: { type: 'date' },
				units: { type: 'int' },
				rating: { type: 'int' },
				active: { type: 'boolean' }
			}
		}
	}
};


//called in dev mode on startup to populate data if db is empty. It's here so it can be easily updated if models are changed.
var populateDB = function (models, logger, callback) {
	logger.info('Attempting to populate DB with test data. Checking whether collections are empty...');
	var saves = [];

	var createUserData = function (cb) {
		models.user.count({}, function (err, count) {
			if (err) { cb(err); return; }
			if (count === 0) {
				logger.info('No users found in dev DB. Creating some sample users.');
				var user0 = new models.user({ firstName: 'Joel', lastName: 'Test0', email: 'test0@domain.com' }),
					user1 = new models.user({ firstName: 'Amos', lastName: 'Test1', email: 'test1@domain.com' }),
					user2 = new models.user({ firstName: 'Micah', lastName: 'Test2', email: 'test2@domain.com' });
				saves.push(user0);
				saves.push(user1);
				saves.push(user2);
			}
			cb(null);
		});
	};

	var createAssetData = function (cb) {
		models.asset.count({}, function (err, count) {
			if (err) { cb(err); return; }
			if (count === 0) {
				logger.info('No assets found in dev DB. Creating some sample assets.');
				var asset0 = new models.asset({ name: 'Asset0', typeCode: 'Code0', typeDescription: 'Asset of type code0', purchaseDate: new Date(), active: true  }),
					asset1 = new models.asset({ name: 'Asset1', typeCode: 'Code1', typeDescription: 'Asset of type code1', purchaseDate: new Date(), active: false  }),
					asset2 = new models.asset({ name: 'Asset1', typeCode: 'Code1', typeDescription: 'Asset of type code2', active: true, units: 3 });
				saves.push(asset0);
				saves.push(asset1);
				saves.push(asset2);
			}
			cb(null);
		});
	};

	async.parallel([createUserData, createAssetData], function (err) {
		if (err) { callback(err); return; }
		if (saves.length === 0) {
			logger.info('All collections are already populated. No test data was added.');
			return;
		}
		async.eachSeries(
			saves,
			function (doc, cb) {
				doc.save(cb);
			},
			function (err) {
				if (err) { callback(err); return; }
				logger.info('Successfully populated DB with ' + saves.length + ' test documents.');
				callback(null);
			}
		);
	});
};





module.exports.user = user;
module.exports.asset = asset;

module.exports.populateDB = populateDB;
module.exports.swaggerModels = swaggerModels;
