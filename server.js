'use strict';
var path = require('path');
var url = require('url');
var async = require('async');
var express = require('express');
var swagger = require('swagger-node-express');
var api = require('./routes/api');
var schemas = require('./schemas');
var config = require('./config').config;
var logger = config.logger;


var app = express(),
	apiApp = express(),
	entityNames = Object.keys(schemas.swaggerModels.models);

app.set('port', config.port);
app.use(express.bodyParser());
app.use('/' + config.apiSubpath, apiApp); //put the api under the configured subpath. 


app.use(express.static(path.join(__dirname, 'static')));
//logger.debug(__dirname);


swagger.setAppHandler(apiApp);
swagger.addModels(schemas.swaggerModels);

async.each(
	entityNames,
	function (entityName, callback) {
		try {
			swagger.addGet(api.getAllEntities(entityName));
			swagger.addGet(api.findEntityById(entityName));
			swagger.addPost(api.addEntity(entityName));
			swagger.addPut(api.updateEntity(entityName));
			swagger.addDelete(api.deleteEntityById(entityName));
		} catch (err) {
			callback(err);
		}
		callback(null);
	},
	function (err) {
		if (err) { logger.error(err); }
	}
);

/* Alternatively, add routes by individual entity	
swagger.addGet(api.getAllEntities('user'));
swagger.addGet(api.getAllEntities('asset'));
swagger.addGet(api.findEntityById('user'));
swagger.addGet(api.findEntityById('asset'));
swagger.addPost(api.addEntity('user'));
swagger.addPost(api.addEntity('asset'));
*/

swagger.configureSwaggerPaths('', '/' + config.apiDocsSubpath, '');
swagger.configure(url.resolve(config.publicUrl, config.apiSubpath), config.apiVersion);


app.listen(config.port);
logger.info('Server listening on port ' + app.get('port'));

//exported for testing purposes
module.exports.app = app;