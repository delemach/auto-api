Auto API
========

## Overview ##

This is reference implementation for a RESTful API centered on entity CRUD operations. The CRUD operations are all automatically generated from the entity definitions. Additionally, the API has swagger built in so it supports active docs through a swagger ui.

To use as is all you need to do is set your configuration values as described below, deploy and start. 

## Demo ##
You can see the reference implementation in action [here](http://apipoc.aws.af.cm/activedocs).
**NOTE:** For the demo the create, update and delete methods are disabled and will _always_ simply return success.

## Usage and Configuration ##
### Environment Setup ###

You need Nodejs installed on your system and in your path. This reference implementation was built using Nodejs version 0.8.16, and is currently updated to and tested against version 0.10.12. See the [downloads](http://nodejs.org/download/) page for versions and install information.

### Dependencies ###

See the npm-shrinkwrap.json file for the full list of packages that are used. That said, the main pieces are:

* [Nodejs](http://nodejs.org) 
* [Express](http://expressjs.com)
* [MongoDB](http://www.mongodb.org/)
* [Mongoose](http://mongoosejs.com/)
* [Swagger](https://github.com/wordnik/swagger-node-express)

From the directory where this reference implementation exists, use `npm install` to setup the dependencies from the shrinkwrap file.

### Configuration Setup ###

In your dev environment, create a "localconfig.js" file, and copy in the contents from the "localconfig.js.example" file. There are only two required configuration values:

* _dbconnstring_ - The database connection information. You can either setup mongodb locally, or use a hosted solution. For example, [mongolab](http://mongolab.com) has a free developer tier that works well.
* _publicUrl_ - The base URL for the deployment. This defaults to 'http://localhost', so it only needs to be explicitly set for production.

There are many other configuration settings, but they all use reasonable defaults. See the "config.js" file for full details. 

For production use, set the process.env.NODE_ENV variable to 'production'. This will cause the app to load all configuration values from environment variables rather than from the "localconfig.js" file.

### Customizing the Entity Definitions ###

The API is code is designed to be very easy to use and to customize - to change the entities you only need to modify the schemas.js file. All RESTful CRUD operations (GET, PUT, POST, DELETE) will be automatically generated along with Swagger documentation for active docs.

### Run the API ###

Run `node server` to run the API. Note if you're in development mode the application will pre-populate the DB with some test data on the first startup.

## Active Docs ##

This reference implementation uses [swagger](https://github.com/wordnik/swagger-node-express) on all api methods, and therefore automatically provides active docs from [any](http://petstore.swagger.wordnik.com/) accessible swagger ui. Just replace URL with the discovery URL of this API.

The discoveryURL is built from the config values in the config.js file. It is: `[publicUrl]/[apiSubpath]/[apiDocsSubpath]`. For example, in development using all default config values the discover URL is: `http://localhost:1337/api/api-docs`

For convenience, this implementation includes its own swagger-ui pulled from the 'dist' folder of the [swagger-ui git repository](https://github.com/wordnik/swagger-ui), and is located under `static/activedocs`. This means that if you keep all default configuration values then during dev the swagger-ui is located at `http://localhost:1337/activedocs`

This swagger UI instance has been only very minorly modified as follows:

* Turned on header parameters as described [here](https://github.com/wordnik/swagger-ui#header-parameters).
* Allowed 'delete' invocation as described [here](https://github.com/wordnik/swagger-ui#http-methods-and-api-invocation).
* Set the discoveryURL to be `/api/api-docs`. For prod deployment you may want to change this to a fully qualified URL just so it's a little clearer, but this is not required.
* Commented out the links in the header to the petstore API and the wordnik API.

## Testing ##

This reference implemenation includes the start of a suite of integration-level tests against the API calls. The tests use [Mocha](http://visionmedia.github.io/mocha/) as the framework, [Chai](http://chaijs.com/) for the assertion library, and [SuperTest](https://github.com/visionmedia/supertest) to make the API calls. 

Right now there's only one simple test written; this reference implemenation just puts the structure in place.  As you customize this starting reference implementation you will need to create your custom test suite as well.

## License ##

Starter App is licensed under by TopCoder. See the accompanying LICENSE.md file.

The swagger-ui found under `static/activedocs` folder remains under [Apache License](http://www.apache.org/licenses/LICENSE-2.0) as stated [here](https://github.com/wordnik/swagger-ui#license).

## Future Work ##

* Adding in API authorization/authentication.
