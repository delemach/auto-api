'use strict';
var mongoose = require('mongoose');
var swagger = require('swagger-node-express');
var JSON = require('JSON');
var schemas = require('./../schemas');
var config = require("./../config").config;

var logger = config.logger,
	models = config.models;


//Generic helper methods
var parseObjectId = function (id) {
	var objectId = null;
	try {
		objectId = mongoose.Types.ObjectId(id);
	} catch (err) { }
	return objectId;
};

//swagger implemented errors poorly. See swagger.js line 515. This works better and provides more info in the response.
var sendErrorResponse = function (res, errorCode, moreInfo) {
	errorCode.details = moreInfo || 'No additional information available.';
	res.send(errorCode, errorCode.value);
};

//Generalized find by ID method with swagger support. The entity name must match the name in the swagger models and the name in the config.models
module.exports.findEntityById = function (entityName) {
	var cappedName = entityName.charAt(0).toUpperCase() + entityName.slice(1);
	var specAndFunction = {
		spec: {
			description: 'Operations on a ' + cappedName,
			path: '/' + entityName + 's/{id}',
			notes: 'Returns a ' + entityName + ' based on an id. Note the id is a MongoDB ObjectID.',
			summary: 'Find ' + entityName + ' by ID',
			method: 'GET',
			params: [swagger.pathParam('id', 'ObjectId of the ' + entityName + ' that needs to be retrieved.', 'string')],
			responseClass: entityName,
			nickname : 'get' + cappedName + 'ById'
		},
		action: function (req, res) {
			var id = parseObjectId(req.params.id);
			if (!id) {
				sendErrorResponse(res, config.apiCodes.badRequest, 'Value \'' + req.params.id + '\' is not a valid MongoDB ObjectID');
				return;
			}
			logger.debug('Retrieving ' + cappedName + ' with id: ' + id);
			models[entityName].findById(id, function (err, entityDoc) {
				if (err) {
					logger.error(err);
					sendErrorResponse(res, config.apiCodes.serverError, 'MongoDB returned an error. An admin will need to investigate.');
					return;
				}
				if (!entityDoc) {
					sendErrorResponse(res, config.apiCodes.notFound, 'No ' + entityName + ' found with id \'' + id + '\'.');
					return;
				}
				res.send(entityDoc);
			});
		}
	};
	return specAndFunction;
};

//Generalized find all with swagger support. The entity name must match the name in the swagger models and the name in the config.models
module.exports.getAllEntities = function (entityName) {
	var cappedName = entityName.charAt(0).toUpperCase() + entityName.slice(1);
	var specAndFunction = {
		spec: {
			description: 'Operations on a ' + cappedName,
			path: '/' + entityName + 's',
			notes: 'Returns all ' + entityName + 's.',
			summary: 'Find all ' + entityName + 's',
			method: 'GET',
			params: [],
			responseClass: entityName,
			nickname : 'getAll' + cappedName
		},
		action: function (req, res) {
			logger.debug('Retrieving all ' + cappedName + 's.');
			models[entityName].find({}, function (err, entityDocs) {
				if (err) {
					logger.error(err);
					sendErrorResponse(res, config.apiCodes.serverError, 'MongoDB returned an error. An admin will need to investigate.');
					return;
				}
				if (!entityDocs) {
					sendErrorResponse(res, config.apiCodes.notFound, 'No ' + entityName + 's were found.');
					return;
				}
				res.send(entityDocs);
			});
		}
	};
	return specAndFunction;
};

//Generalized insert method with swagger support. The entity name must match the name in the swagger models and the name in the config.models
module.exports.addEntity = function (entityName) {
	var cappedName = entityName.charAt(0).toUpperCase() + entityName.slice(1);
	var specAndFunction = {
		spec: {
			description: 'Operations on a ' + cappedName,
			path: '/' + entityName + 's',
			notes: 'Creates a new ' + entityName + '. Enclose all parameter names, strings, and date strings in "double-quotes". Leave out the "_id" parameter since it will be automtically assigned by the DB.',
			summary: 'Creates a new ' + entityName,
			method: 'POST',
			params: [swagger.postParam(entityName, 'JSON of the new ' + entityName + ' to create.')],
			nickname : 'create' + cappedName
		},
		action: function (req, res) {
			var newEntity = req.body;
			if (!newEntity || Object.keys(newEntity).length === 0) {
				sendErrorResponse(res, config.apiCodes.badRequest, 'JSON for the ' + entityName + ' to create is missing from the body of the request.');
				return;
			}
			logger.debug('About to create: ' + newEntity);
			models[entityName].create(newEntity, function (err, newDoc) {
				if (err) {
					logger.error(err);
					sendErrorResponse(res, config.apiCodes.serverError, 'MongoDB returned an error. An admin will need to investigate.');
					return;
				}
				res.send(newDoc._id);
			});
		}
	};
	return specAndFunction;
};

//Generalized update method with swagger support. The entity name must match the name in the swagger models and the name in the config.models
//NOTE that per mongoose docs, schema validation rules are not applied when using the model.update method!
module.exports.updateEntity = function (entityName) {
	var cappedName = entityName.charAt(0).toUpperCase() + entityName.slice(1),
		conditions = {};

	var specAndFunction = {
		spec: {
			description: 'Operations on a ' + cappedName,
			path: '/' + entityName + 's',
			notes: 'Updates an existing ' + entityName + '. Enclose all parameter names, strings, and date strings in "double-quotes".',
			summary: 'Updates an existing ' + entityName,
			method: 'PUT',
			params: [swagger.postParam(entityName, 'JSON of the ' + entityName + ' to update. "_id" must be specified and exist.')],
			nickname : 'edit' + cappedName
		},
		action: function (req, res) {
			var modifiedEntity = req.body;
			if (!modifiedEntity || Object.keys(modifiedEntity).length === 0) {
				sendErrorResponse(res, config.apiCodes.badRequest, 'JSON for the ' + entityName + ' to update is missing from the body of the request.');
				return;
			}
			if (!modifiedEntity._id || !parseObjectId(modifiedEntity._id)) {
				sendErrorResponse(res, config.apiCodes.badRequest, 'The _id for the ' + entityName + ' to update is missing or is not a valid MongoDB ObjectID.');
				return;
			}
			logger.debug('About to update: ' + modifiedEntity);
			conditions._id = modifiedEntity._id;
			delete modifiedEntity._id;
			models[entityName].update(conditions, modifiedEntity, function (err, numberAffected) {
				if (err || numberAffected > 1) {
					logger.error(err);
					sendErrorResponse(res, config.apiCodes.serverError, 'MongoDB returned an error. An admin will need to investigate.');
					return;
				}
				if (numberAffected === 0) {
					sendErrorResponse(res, config.apiCodes.notFound, 'No ' + entityName + ' was found with the specified _id.');
					return;
				}
				res.send(config.apiCodes.OK);
			});
		}
	};
	return specAndFunction;
};


//Generalized delete by ID method with swagger support. The entity name must match the name in the swagger models and the name in the config.models
module.exports.deleteEntityById = function (entityName) {
	var cappedName = entityName.charAt(0).toUpperCase() + entityName.slice(1);
	var specAndFunction = {
		spec: {
			description: 'Operations on a ' + cappedName,
			path: '/' + entityName + 's/{id}',
			notes: 'Deletes a ' + entityName + ' based on an id. Note the id is a MongoDB ObjectID.',
			summary: 'Delete ' + entityName + ' by ID',
			method: 'DELETE',
			params: [swagger.pathParam('id', 'ObjectId of the ' + entityName + ' that needs to be deleted.', 'string')],
			responseClass: entityName,
			nickname : 'remove' + cappedName + 'ById'
		},
		action: function (req, res) {
			var id = parseObjectId(req.params.id);
			if (!id) {
				sendErrorResponse(res, config.apiCodes.badRequest, 'Value \'' + req.params.id + '\' is not a valid MongoDB ObjectID');
				return;
			}
			logger.debug('Deleting ' + cappedName + ' with id: ' + id);
			models[entityName].remove({_id : id}, function (err) {
				if (err) {
					logger.error(err);
					sendErrorResponse(res, config.apiCodes.serverError, 'MongoDB returned an error. An admin will need to investigate.');
					return;
				}
				res.send(config.apiCodes.OK);
			});
		}
	};
	return specAndFunction;
};
