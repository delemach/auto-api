'use strict';
var mongoose = require('mongoose');
var async = require('async');
var schemas = require('./schemas');
var winston = require('winston');
var localconfig = null;
var entityNames = Object.keys(schemas.swaggerModels.models);

//default to development
var environment = process.env.NODE_ENV || process.env.CI || 'development';
if (environment === 'development') {
	localconfig = require("./localconfig").localconfig;
}

//Helper function for setting config values
function getConfigValue(configName) {
	if (environment === 'development') {
		return localconfig[configName];
	}
	else { //both production and CI are assumed to use environment variables
		return process.env[configName];
	}
	throw new Error('Cannot load configuration values since the environment "' + environment + '" is unknown.');
}

//All application wide values are parsed into a 'config' object.
var config =	{
		//MUST BE SET TO CUSTOM VALUE FOR PRODUCTION AND DEVELOPMENT. FOR DEV, SET IT IN YOUR localconfig.js FILE.
		dbconnstring: getConfigValue('dbconnstring'),

		//MUST BE SET TO CUSTOM VALUE FOR PRODUCTION USE. FEEL FREE TO OVERRIDE DEFAULT DEV VALUES IN YOUR localconfig.js FILE.
		publicUrl   : getConfigValue('publicUrl') || 'http://localhost', //used only by swagger for the active api docs
		port: getConfigValue('VMC_APP_PORT') || getConfigValue('PORT') || 1337, //Note if you're using a PAAS service like appfog for prod this environment variable is set for you automatically, VMC_APP_PORT is used by Appfog, PORT by Heroku. 

		//ALL REMAINING CONFIG VALUES ARE OPTIONAL FOR BOTH DEV AND PROD AND HAVE SENSIBLE DEFAULTS.
		apiSubpath  : getConfigValue('apiSubpath') || 'api',
		apiDocsSubpath : getConfigValue('apiDocsSubpath') || 'api-docs',
		apiVersion : '0.1',
		logger: new (winston.Logger)({
			transports: [
				new (winston.transports.Console)({ json: true, colorize: true, prettyPrint: true}),
				new (winston.transports.File)({ filename: getConfigValue('logFileName') || 'logfile.log' })
			]
		})
	};

//do some config cleanup
config.port = parseInt(String(config.port).replace(/:/, ''), 10); //remove the colon if it was put in the config value
config.apiSubpath = config.apiSubpath.replace(/\//g, ''); //remove slashes if they were put in the api subpath
config.apiDocsSubpath = config.apiDocsSubpath.replace(/\//g, ''); //remove slashes if they were put in the api subpath
config.publicUrl = config.publicUrl.replace(/\/$/, ''); //remove trailing slash if it was put in the public url

//For dev environment only, assume the port has to be part of the public url
if (environment === 'development' && config.port !== 80) {
	config.publicUrl = config.publicUrl + ':' + config.port;
}

// Add api response codes. This is a subset of codes following this convention: https://dev.twitter.com/docs/error-codes-responses
config.apiCodes = {
	OK           : {name : 'OK', value : 200, description : 'Success' },
	notModified  : {name : 'Not Modified', value: 304, description : 'There was no new data to return.' },
	badRequest   : {name : 'Bad Request', value: 400, description : 'The request was invalid. An accompanying message will explain why.' },
	unauthorized : {name : 'Unauthorized', value: 401, description : 'Authentication credentials were missing or incorrect.' },
	forbidden    : {name : 'Forbidden', value: 403, description : 'The request is understood, but it has been refused or access is not allowed.' },
	notFound     : {name : 'Not Found', value: 404, description : 'The URI requested is invalid or the requested resource does not exist.' },
	serverError  : {name : 'Internal Server Error', value: 500, description : 'Something is broken. Please contact support.' }
};

// Setup application wide DB usage. 
var db = mongoose.createConnection(config.dbconnstring);
config.models = {};

//TODO: db connection held open forever?
db.on('error', function (err) {
	config.logger.error('Failed to connect to DB: ' + err);
});
db.once('open', function () {
	async.each(
		entityNames,
		function (entityName, callback) {
			try {
				config.models[entityName] = db.model(entityName, schemas[entityName]);
			} catch (err) {
				callback(err);
			}
			callback(null);
		},
		function (err) {
			if (err) { config.logger.error(err); }
		}
	);

	config.logger.info('Successfully connected to DB and created the document models.');

	//In Dev mode, if DB is empty, populate some test data
	if (environment === 'development') {
		schemas.populateDB(config.models, config.logger, function (err) {
			if (err) {
				config.logger.error('Failed to populate db with test data.' + err);
			}
		});
	}

});



module.exports.config = config;